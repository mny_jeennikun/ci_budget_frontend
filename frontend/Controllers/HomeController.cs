﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace frontend.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Ticket()
        {
            return View();
        }

        #region Menu Company
        public IActionResult Site()
        {
            return View();
        }
        public IActionResult Role()
        {
            return View();
        }
        public IActionResult Corp()
        {
            return View();
        }
        public IActionResult Dept()
        {
            return View();
        }
        public IActionResult Sect()
        {
            return View();
        }
        public IActionResult Job()
        {
            return View();
        }
        public IActionResult Period()
        {
            return View();
        }
        public IActionResult Proj()
        {
            return View();
        }
        public IActionResult Budget_Chart()
        {
            return View();
        }
        public IActionResult Activity()
        {
            return View();
        }
        
        public IActionResult CorpUser() => View();
        #endregion Menu Company

        public IActionResult Error()
        {
            return View();
        }
    }
}
