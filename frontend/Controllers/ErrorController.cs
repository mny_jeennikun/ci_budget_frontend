using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace frontend.Controllers
{
    [Route("Error")]
    public class ErrorController : Controller
    {
        public IActionResult Index()
        {
            return View("~/Views/Shared/Error404.cshtml");
        }
        [Route("404")]
        public IActionResult Error404()
        {
            return View("~/Views/Shared/Error404.cshtml");
        }
        [Route("500")]
        public IActionResult Error500()
        {
            return View("~/Views/Shared/Error500.cshtml");
        }
    }
}