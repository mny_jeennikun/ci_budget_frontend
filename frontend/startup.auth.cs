﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using frontend.Middleware;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

namespace frontend
{
    public partial class Startup
    {
        public static SymmetricSecurityKey signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
        private static readonly string secretKey = "!XC!t$kEY!P@$$KEY";
        private string issuer = "connectworkflow.cloud";
        private string audience = "user";

        private void ConfigureServicesJwt(IServiceCollection services)
        {

            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = issuer,

                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = audience,

                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                        options.Cookie.Name = "access_token";
                        options.LoginPath = "/Account/Login";
                        options.LogoutPath = "/Account/Logout";
                        options.TicketDataFormat = new CustomJwtDataFormat(
                                SecurityAlgorithms.HmacSha256,
                                tokenValidationParameters);
                    })
                     .AddJwtBearer(options =>
                     {
                         options.RequireHttpsMetadata = false;
                         options.IncludeErrorDetails = true;
                         options.Authority = issuer;
                         options.TokenValidationParameters = new TokenValidationParameters
                         {
                             // The signing key must match!
                             ValidateIssuerSigningKey = true,
                             IssuerSigningKey = signingKey,

                             // Validate the JWT Issuer (iss) claim
                             ValidateIssuer = true,
                             ValidIssuer = issuer,

                             // Validate the JWT Audience (aud) claim
                             ValidateAudience = true,
                             ValidAudience = audience,

                             // Validate the token expiry
                             ValidateLifetime = true,

                             // If you want to allow a certain amount of clock drift, set that here:
                             ClockSkew = TimeSpan.Zero
                         };
                     });
        }

        private void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }

    }
}
