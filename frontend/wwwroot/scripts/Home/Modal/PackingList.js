﻿var packing = {
    init: function () {
        var self = this;
        self.event();
    },
    event: function () {
        var self = this;
        self.equal.control.modal.find('a.pallet').click(function (e) {
            e.preventDefault();
            self.equal.sort();
        });
        self.separated.control.modal.find('a.pallet').click(function (e) {
            e.preventDefault();
            self.separated.sort();
        });
        self.control.modal.find('[name=EQUAL]').click(function (e) {
            e.preventDefault();
            var ID = sale.control.addForm.find('[name=INVOICE_ID]').val();
            self.equal.detail(ID);
            self.control.modal.modal('hide');
        });
        self.control.modal.find('[name=SEPARATED]').click(function (e) {
            e.preventDefault();
            var ID = sale.control.addForm.find('[name=INVOICE_ID]').val();
            self.separated.detail(ID);
            self.control.modal.modal('hide');
        });
        self.separated.control.form.find('[name=ADD]').click(function (e) {
            e.preventDefault();
            self.separated.row.add().appendTo(self.separated.control.form.find('tbody'))
            self.separated.reSeq();
        });
        self.equal.control.form.sendForm({
            before: function () {
                return true;
            },
            submit: function (e) {
                self.equal.save();
            }
        });
        self.separated.control.form.sendForm({
            before: function () {
                return true;
            },
            submit: function (e) {
                self.separated.save();
            }
        });
    },
    control: {
        modal: $('#modal-packing'),
        form: $('#modal-packing')
    },
    load: function (ID, TYPE) {
        var self = this;
        if (TYPE == 1) {
            self.control.modal.find('[name=EQUAL]').removeAttr('disabled', 'disabled');
            self.control.modal.find('[name=SEPARATED]').attr('disabled', 'disabled');
        } else if (TYPE == 2) {
            packing.control.modal.find('[name=EQUAL]').attr('disabled', 'disabled');
            self.control.modal.find('[name=SEPARATED]').removeAttr('disabled', 'disabled');
        } else {
            self.control.modal.find('[name=EQUAL]').removeAttr('disabled', 'disabled');
            self.control.modal.find('[name=SEPARATED]').removeAttr('disabled', 'disabled');
        }
    },
    equal: {
        control: {
            modal: $('#modal-packing-equal'),
            form: $('#modal-packing-equal form'),
        },
        PACKING_LIST_TYPE: 1,
        sum: function () {
            var self = this;
            var TOTAL_NET_WEIGHT = 0;
            var TOTAL_GROSS_WEIGHT = 0;
            self.control.form.find('[name=NET_WEIGHT]').each(function (i, e) {
                TOTAL_NET_WEIGHT += parseFloat($(e).val().replace(',', ''));
            });
            self.control.form.find('[name=GROSS_WEIGHT]').each(function (i, e) {
                TOTAL_GROSS_WEIGHT += parseFloat($(e).val().replace(',', ''));
            });
            self.control.form.find('[name=TOTAL_NET_WEIGHT]').val(TOTAL_NET_WEIGHT);
            self.control.form.find('[name=TOTAL_GROSS_WEIGHT]').val(TOTAL_GROSS_WEIGHT);
        },
        detail: function (ID) {
            var self = this;
            setTimeout(function () {
                self.control.form.find('[name=INVOICE_ID]').val(sale.control.addForm.find('[name=INVOICE_ID]').val());
                self.control.form.find('[name=REFNO]').val(sale.control.addForm.find('[name=REFNO]').val());
                self.control.form.find('[name=DOCDATE]').val(sale.control.addForm.find('[name=DOCDATE]').val());
                self.control.form.find('[name=CUSTOMER_ID]').val(sale.control.addForm.find('[name=CUSTOMER_ID]').val());
                self.control.form.find('[name=CUSTOMER_CODE]').val(sale.control.addForm.find('[name=CUSTOMER_CODE]').val());
            });
            self.control.form.find('tbody').html("");
            $.ajax({
                url: "/Api/Document/Detail/Packing/Equal",
                data: { ID: ID },
                success: function (ret) {
                    if (ret.Result == "OK") {
                        setTimeout(function () {
                            $.each(ret.Records.Detail, function (i, e) {
                                var templete = $('#rowEqualTemplate').html();
                                var tr = $('<tr>').html(templete);
                                setTimeout(function () {
                                    tr.find('[name=SEQ],[data-seq="SEQ"]').val(e.SEQ).text(e.SEQ);
                                    $.each(e, function (x, y) {
                                        tr.find('[name=' + x + ']').val(y);
                                    });
                                });
                                self.control.form.find('tbody').append(tr);
                            });
                        })
                        setTimeout(function () {
                            self.control.form.find('tbody tr').find('[name=NET_WEIGHT], [name=GROSS_WEIGHT]').keyup(function () { self.sum() }).number(true, 2);
                            self.control.form.trigger('change');
                        });
                        self.control.modal.modal('show');

                    } else {
                        app.dialogue.warning('', ret.Message);
                    }
                }
            });
        },
        save: function (callback) {
            var self = this;
            var data = self.control.form.formToJson("LIST", "tbody tr");
            data.PACKING_LIST_TYPE = self.PACKING_LIST_TYPE;
            $.ajax({
                url: "/Api/Document/Save/Packing/Equal",
                data: data,
                success: function (ret) {
                    if (ret.Result == "OK") {
                        setTimeout(function () {
                            self.detail(ret.Data);
                        });
                        app.dialogue.success(
                            'Message from system.',
                            'Success.', function () { });
                    } else {
                        app.dialogue.warning('', ret.Message);
                    }
                }
            });
        },
        sort: function () {
            var self = this;
            var objs = [];
            self.control.form.find('tbody tr').each(function (i, e) {
                var obj = {
                    KEY: app.guid(),
                    PALLET_NUMBER: $(e).find('[name=PALLET_NUMBER]').val(),
                    ELEMENT: $(e)
                }
                $(e).attr('key', obj.KEY);
                objs.push(obj);
            });
            function compare(a, b) {
                if (a.PALLET_NUMBER < b.PALLET_NUMBER)
                    return -1;
                else if (a.PALLET_NUMBER > b.PALLET_NUMBER)
                    return 1;
                else
                    return 0;
            }
            var arr = objs.sort(compare);
            for (var i = 0; i < arr.length; i++) {
                var tr = self.control.form.find('tbody tr[key=' + arr[i].KEY + ']');
                tr.find('[name=SEQ]').val(i + 1);
                tr.find('[data-seq="SEQ"]').text(i + 1);
                if ((arr.length - 1) == i) {
                    tr.after(self.control.form.find('tbody tr:last-child'));
                } else {
                    tr.after(self.control.form.find('tbody tr[key=' + arr[i + 1].KEY + ']'));
                }
            }
        }
    },
    separated: {
        control: {
            modal: $('#modal-packing-separated'),
            form: $('#modal-packing-separated form'),
        },
        PACKING_LIST_TYPE: 2,
        detail: function (ID) {
            var self = this;
            Pace.restart();
            self.control.form.find('[name=INVOICE_ID]').val(sale.control.addForm.find('[name=INVOICE_ID]').val());
            self.control.form.find('[name=REFNO]').val(sale.control.addForm.find('[name=REFNO]').val());
            self.control.form.find('[name=DOCDATE]').val(sale.control.addForm.find('[name=DOCDATE]').val());
            self.control.form.find('[name=CUSTOMER_ID]').val(sale.control.addForm.find('[name=CUSTOMER_ID]').val());
            self.control.form.find('[name=CUSTOMER_CODE]').val(sale.control.addForm.find('[name=CUSTOMER_CODE]').val());
            self.control.form.find('tbody').html("");
            $.ajax({
                url: "/Api/Document/Detail/Packing/Separated",
                data: { ID: ID },
                success: function (ret) {
                    if (ret.Result == "OK") {
                        $.each(ret.Records.Header, function (i, e) {
                            if (i == "DUEDATE" || i == "DOCDATE") {
                                self.control.form.find('[name=' + i + ']').val(moment(e).format("DD/MM/YYYY"));

                            } else {
                                self.control.form.find('[name=' + i + ']').val(e);
                            }
                        });
                        if (ret.Records.Detail.length < 1) {
                            self.row.add().appendTo(self.control.form.find('tbody'));
                        } else {
                            $.each(ret.Records.Detail, function (i, e) {
                                self.row.add(e).appendTo(self.control.form.find('tbody'));
                            });
                        }
                        self.control.modal.modal('show');
                        setTimeout(function () {
                            self.control.form.find('tbody tr [name=QTY]').trigger('keyup');
                            self.sort();
                            app.initDateTime();
                        });
                    } else {
                        app.dialogue.warning('', ret.Message);
                    }
                }
            });
            setTimeout(function () {
                $.ajax({
                    url: "/Api/Document/Detail/Packing/DefaultPackingDescription",
                    data: { ID: ID },
                    success: function (ret) {
                        if (ret.Result == "OK") {
                            self.defaultPacking = ret.Data;
                        }
                    }
                });
            });
            self.control.form.trigger('change');
        },
        row: {
            add: function (obj) {
                var self = packing.separated;
                var templete = $('#rowSeparatedTemplate').html();
                var tr = $('<tr/>').html(templete);
                tr.find('[name=PACKING_ITEM_CODE]').click(function () {
                    var option = app.listModal.options.itemPackingSeparated;
                    option.select = false;
                    app.listModal.init(option);
                    app.listModal.load(option);
                    app.listModal.control.tempCallback = function (data) {
                        Pace.restart();
                        $.each(data, function (i, e) {
                            tr.find('[name=' + i + ']').val(e);
                        });
                        tr.find('[name=PACKING_ITEM_DESCRIPTION]').val(self.defaultPacking);
                        self.control.form.find('tbody tr:last-child [name=QTY]').trigger('keyup');
                        self.sort();
                        app.listModal.control.tempModal.modal('hide');
                    };
                    app.listModal.control.tempModal.modal('show');
                });
                tr.find('[name=QTY], [name=QTY_2], [name=QTY_3]').number(true, 0);
                tr.find('[name=NET_WEIGHT], [name=GROSS_WEIGHT], [name=ITEM_AMTKE_FOB], [name=ITEM_AMTKE_CIF]').number(true, 2);
                tr.find('[name=QTY], [name=QTY_2], [name=QTY_3],[name=NET_WEIGHT], [name=GROSS_WEIGHT], [name=ITEM_AMTKE_FOB], [name=ITEM_AMTKE_CIF]')
                    .on('keyup', function (e) {
                        e.preventDefault();
                        var totalQTY = 0;
                        var totalQTY2 = 0;
                        var totalQTY3 = 0;
                        var netWeight = 0;
                        var gotWeight = 0;
                        var amtkeFOB = 0;
                        var amtkeCIF = 0;
                        self.control.form.find('tbody tr').each(function (k, v) {
                            totalQTY += parseFloat($(v).find('[name=QTY]').val().replace(',', ''));
                            totalQTY2 += parseFloat($(v).find('[name=QTY_2]').val().replace(',', ''));
                            totalQTY3 += parseFloat($(v).find('[name=QTY_3]').val().replace(',', ''));
                            netWeight += parseFloat($(v).find('[name=NET_WEIGHT]').val().replace(',', ''));
                            gotWeight += parseFloat($(v).find('[name=GROSS_WEIGHT]').val().replace(',', ''));
                            amtkeFOB += parseFloat($(v).find('[name=ITEM_AMTKE_FOB]').val().replace(',', ''));
                            amtkeCIF += parseFloat($(v).find('[name=ITEM_AMTKE_CIF]').val().replace(',', ''));
                        });
                        self.control.form.find('[name=TOTAL_QTY]').val(totalQTY);
                        self.control.form.find('[name=TOTAL_QTY2]').val(totalQTY2);
                        self.control.form.find('[name=TOTAL_QTY3]').val(totalQTY3);
                        self.control.form.find('[name=TOTAL_NET_WEIGHT]').val(netWeight);
                        self.control.form.find('[name=TOTAL_GROSS_WEIGHT]').val(gotWeight);
                        self.control.form.find('[name=TOTAL_AMTKE_FOB]').val(amtkeFOB);
                        self.control.form.find('[name=TOTAL_AMTKE_CIF]').val(amtkeCIF);
                    });
                tr.find('button[name=REMOVE_RECORD]').on('click', function (e) {
                    e.preventDefault();
                    if (self.control.form.find('tbody tr').length <= 1) {
                        app.dialogue.warning('Message from system.', 'Minimum 1 row.');
                    } else {
                        tr.remove();
                        self.sort();
                    }
                });
                if (obj != undefined) {
                    tr.find('[name]').each(function (x, y) {
                        var name = $(y).attr('name');
                        var value = obj[name];
                        if (value != undefined) {
                            tr.find('[name=' + name + ']').val(value);
                        }
                    });
                }
                return tr;
            },
            select: function (tr) {
                app.listModal.reset();
                var self = sale;
                if (self.control.addForm.find('[name=CUSTOMER_ID]').val() == "") {
                    app.dialogue.warning('Message from system.', 'Please select customer.', function () {
                        self.control.addForm.find('[name=CUSTOMER_CODE]').trigger('click');
                    });
                } else {
                    var option = app.listModal.options.itemByRefType;
                    option.REFTYPE_ID = self.REFTYPE_ID;
                    option.CORP_ID = corpBranch.data().CORP_ID;
                    option.BRANCH_ID = corpBranch.data().BRANCH_ID;
                    option.CUSTOMER_ID = self.control.addForm.find('[name=CUSTOMER_ID]').val();
                    option.select = false;
                    option.INVOICE_ITEM_ID = self.modal.getInvoiceItem();
                    app.listModal.init(option);
                    app.listModal.load(option);
                    app.listModal.control.tempCallback = function (data) {
                        $.each(data, function (i, e) {
                            tr.find("[name=" + i + "]").val(e).text(e).trigger('change');
                        });
                        app.listModal.control.tempModal.modal('hide');
                    };
                    app.listModal.control.tempModal.modal('show');
                }

            },
        },
        save: function (callback) {
            var self = this;
            var data = self.control.form.formToJson("DETAIL", "tbody tr");
            data.PACKING_LIST_TYPE = self.PACKING_LIST_TYPE;
            $.ajax({
                url: "/Api/Document/Save/Packing/Separated",
                data: data,
                success: function (ret) {
                    if (ret.Result == "OK") {
                        setTimeout(function () {
                            self.detail(ret.Data);
                        });
                        app.dialogue.success(
                            'Message from system.',
                            'Success.', function () { });
                    } else {
                        app.dialogue.warning('', ret.Message);
                    }
                }
            });
        },
        sort: function () {
            var self = this;
            var objs = [];
            self.control.form.find('tbody tr').each(function (i, e) {
                var obj = {
                    KEY: app.guid(),
                    PALLET_NUMBER: $(e).find('[name=PALLET_NUMBER]').val(),
                    ELEMENT: $(e)
                }
                $(e).attr('key', obj.KEY);
                objs.push(obj);
            });
            function compare(a, b) {
                if (a.PALLET_NUMBER < b.PALLET_NUMBER)
                    return -1;
                else if (a.PALLET_NUMBER > b.PALLET_NUMBER)
                    return 1;
                else
                    return 0;
            }
            var arr = objs.sort(compare);
            for (var i = 0; i < arr.length; i++) {
                var tr = self.control.form.find('tbody tr[key=' + arr[i].KEY + ']');
                tr.find('[name=SEQ]').val(i + 1);
                tr.find('[data-seq="SEQ"]').text(i + 1);
                if ((arr.length - 1) == i) {
                    tr.after(self.control.form.find('tbody tr:last-child'));
                } else {
                    tr.after(self.control.form.find('tbody tr[key=' + arr[i + 1].KEY + ']'));
                }
            }
        },
        reSeq: function () {
            var self = this;
            self.control.form.find('tbody tr').each(function (i, e) {
                $(e).find('[name=SEQ]').val(i + 1);
                $(e).find('[data-seq="SEQ"]').text(i + 1);
            });
        },
        defaultPacking: "",
    },
}