﻿var main = {
    init: function () {
        var self = this;
        self.tableMain.init();
        main.formAdd.init();
        self.event();
    },
    control: {
        btnAdd: $('#btnAdd'),
        formMain: $('#formMain'),
        tableMain: $('#tableMain'),
        formAdd: $('#formAdd'),
        modalAdd: $('#modalAdd'),
    },
    event: function () {
        var self = this;
        self.control.formMain.on('reset', function (e) {
            self.tableMain.load();
        });
        self.control.formMain.submit(function (e) {
            e.preventDefault();
            self.control.formMain.find('div.overlay').hide();
            self.tableMain.load();
        });

    }
}

main.tableMain = {
    init: function () {
        var self = main;
        self.control.tableMain.jtable({
            paging: true,
            pageSize: 10,
            sorting: true,
            saveUserPreferences: false,
            defaultSorting: 'SITE_CODE ASC',
            actions: {
                listAction: '/Api/Site/List',
            },
            fields: {
                SITE_ID: {
                    list: false,
                    key: true
                },
                RECORD: {
                    title: '#',
                    sorting: false,
                    width: "3%",
                    listClass: "text-center"
                },
                SITE_CODE: {
                    title: 'Site Code.',
                    width: "10%"
                },
                SITE_NAME: {
                    title: 'Site Name',
                    width: '60%'
                }
            },
            recordsLoaded: function (event, data) {
                self.control.tableMain.find('.jtable-data-row').click(function (e) {
                    e.preventDefault();
                    var data = $(this).data();
                    self.formAdd.load(data.recordKey);
                });
            }
        });
        self.tableMain.load();
    },
    load: function () {
        var self = main;
        self.control.tableMain.jtable('load', self.control.formMain.serialize());
    }
}
main.formAdd = {
    init: function () {
        var self = main;
        main.formAdd.event();
    },
    event: function () {
        var self = main;
        self.control.btnAdd.on('click', function (e) {
            e.preventDefault();
            self.control.formAdd[0].reset();
            self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').removeAttr('disabled', 'disabled');
            self.control.formAdd.find('button[value=edit]').hide();
            self.control.formAdd.find('button[value=save]').show();
            self.control.modalAdd.modal('show');
        });
        self.control.formAdd.find('button[value=edit]').on('click', function (e) {
            e.preventDefault();
            self.control.formAdd.find('button[value=edit]').hide();
            self.control.formAdd.find('button[value=save]').show();
            self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').removeAttr('disabled', 'disabled');

        });
        self.control.formAdd.sendForm({
            before: function () {

            },
            submit: function () {
                self.formAdd.save();
            },
            reset: function () {
                self.control.formAdd.find('button[value=edit]').show();
                self.control.formAdd.find('button[value=save]').hide();
                self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').attr('disabled', 'disabled');
            }
        });
    },
	load: function (id) {
		console.log('/Api/Site/' + id);
        var self = main;
        main.control.formAdd[0].reset();
        self.control.formAdd.find('div.overlay').show();
        self.control.modalAdd.modal('show');
        $.ajax({
            url: '/Api/Site/' + id,
            type: 'GET',
            success: function (ret) {
                if (ret.Result === "OK") {
                    $.each(ret.Data, function (i, e) {
                        self.control.formAdd.find('[name=' + i + ']').val(e).text(e);
                    });
                } else {
                    dialogue.warning('', ret.Message, function () {
                        self.control.formAdd.modal('hide');
                    });
                }
                self.control.formAdd.find('button[value=edit]').show();
                self.control.formAdd.find('button[value=save]').hide();
                self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').attr('disabled', 'disabled');
                self.control.formAdd.find('div.overlay').hide();
            }
        })
    },
    save: function () {
        var self = main;
		self.control.formAdd.find('div.overlay').show();
		//console.log(self.control.formAdd.serialize());
        $.ajax({
            url: '/Api/Site/Save',
            data: self.control.formAdd.serialize(),
            success: function (ret) {
                if (ret.Result === "OK") {
                    dialogue.success();
                    self.tableMain.load();
                    self.formAdd.load(ret.Data);
                } else {
                    dialogue.warning('', ret.Message, function () {
                        self.control.formAdd.modal('hide');
                    });
                }
                self.control.formAdd.find('div.overlay').hide();
            }
        });
    }
}

$(document).ready(function () {
    main.init();
});