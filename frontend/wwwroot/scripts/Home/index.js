﻿var main = {
    init: function () {
        var self = this;
        //self.loadEach();
        self.load();
        self.event();
    },
    control: {
        mainTimeLine: $('#mainTimeline'),
        endTimeline: $('#endTimeline'),
        formAdd: $('#formAdd'),
        modalAdd: $('#modalAdd'),
    },
    event: function () {
        var self = this;
    },
    load: function () {
        var self = this;
        var t = timer('For.in');
        for (var x = 0; x < 1000; x++) {
            for (index in data) {
                (function (i, e) {
                    var timeline = main.template.timeline();
                    setAsync(function () {
                        for (key in e) {
                            timeline.find('[data-bind=' + key + ']').text(e[key])
                        }
                        timeline.find('button').on('click', function (x) {
                            x.preventDefault();
                            alert(e["Header"]);
                        });
                    })
                    timeline.insertBefore(self.control.endTimeline);
                })(index, data[index]);
            }
        }
        t.stop();
    },
    loadEach: function () {
        var self = this;
        var t = timer('jQuery.Each');
        for (var x = 0; x < 100; x++) {
            $.each(data, function (i, e) {
                var timeline = main.template.timeline();
                setTimeout(function () {
                    for (key in e) {
                        timeline.find('[data-bind=' + key + ']').text(e[key])
                    }
                    timeline.find('button').on('click', function (x) {
                        x.preventDefault();
                        alert(e["Header"]);
                    });
                })
                timeline.insertBefore(self.control.endTimeline);
            });
        }
        t.stop();
    },
    template: {
        timeline: function () {
            return $('<li/>').append($('#templateTimeline').html());
        },
        timelineLabel: function () {
            return $(document.createElement('li')).append($('#templateTimelineLabel').html()).addClass('time-label');
        },
    }
}
function setAsync(callback) {
    setTimeout(function () {
        callback();
    });
}
var timer = function (name) {
    var start = new Date();
    return {
        stop: function () {
            var end = new Date();
            var time = end.getTime() - start.getTime();
            console.log('Timer:', name, 'finished in', time, 'ms');
        }
    }
};

$(document).ready(function () {
    main.init();
});