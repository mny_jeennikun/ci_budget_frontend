﻿var main = {
    init: function () {
        var self = this;
        self.tableMain.init();
        main.formAdd.init();
        groupList.init();
        self.event();
    },
    control: {
        btnAdd: $('#btnAdd'),
        formMain: $('#formMain'),
        tableMain: $('#tableMain'),
        formAdd: $('#formAdd'),
        modalAdd: $('#modalAdd'),
        tempCallback: undefined
    },
    event: function () {
        var self = this;
        self.control.formMain.on('reset', function (e) {
            self.tableMain.load();
        });
        self.control.formMain.find('[name=PERIOD_CODE]').on('click', function (e) {
            e.preventDefault();
            var option = app.listModal.options.period;
            app.listModal.init(option);
            app.listModal.load(option);
            app.listModal.control.tempCallback = function (ret) {
                self.control.formMain.find('[name=SITE_ID]').val(ret.SITE_ID);
                self.control.formMain.find('[name=CORP_ID]').val(ret.CORP_ID);
                self.control.formMain.find('[name=PERIOD_ID]').val(ret.PERIOD_ID);
                self.control.formMain.find('[name=PERIOD_CODE]').val(ret.PERIOD_CODE);
            }
            app.listModal.show();

        });
        self.control.formMain.submit(function (e) {
            e.preventDefault();
            self.tableMain.load();

            self.control.formMain.find('div.overlay').hide();
        });

    },
    
}

main.tableMain = {
    init: function () {
        var self = main;
        self.control.tableMain.jtable({
            paging: true,
            pageSize: 10,
            sorting: true,
            saveUserPreferences: false,
            defaultSorting: 'ACTIVITY_CODE ASC',
            actions: {
                listAction: '/Api/Activity/List',
            },
            fields: {
                ACTIVITY_ID: {
                    list: false,
                    key: true
                },
                RECORD: {
                    title: '#',
                    sorting: false,
                    width: "3%",
                    listClass: "text-center"
                },
                DEPT_CODE: {
                    title: 'Dept Code.',
                    width: "10%",
                    listClass: "text-center",
                    display: function (data) {
                        return data.record.DEPT_CODE;
                    }
                },
                SECT_CODE: {
                    title: 'Sect Code.',
                    width: "10%",
                    listClass: "text-center",
                    display: function (data) {
                        return data.record.SECT_CODE;
                    }
                },
                PROJ_CODE: {
                    title: 'Project Code.',
                    width: "10%",
                    listClass: "text-center",
                    display: function (data) {
                        return data.record.PROJ_CODE;
                    }
                },
                JOB_CODE: {
                    title: 'Sub-Project Code.',
                    width: "10%",
                    listClass: "text-center",
                    display: function (data) {
                        return data.record.JOB_CODE;
;
                    }
                },
                ACTIVITY_CODE: {
                    title: 'Activity Code.',
                    width: "10%",
                    listClass: "text-center"
                },
                ACTIVITY_NAME_EN: {
                    title: 'Activity Name',
                    width: "10%",
                    listClass: "text-center"
                },
                TOTAL_BUDGET_AMT: {
                    title: 'Amount',
                    width: "10%",
                    listClass: "text-center"
                },
            },
            recordsLoaded: function (event, data) {
                self.control.tableMain.find('.jtable-data-row').click(function (e) {
                    e.preventDefault();
                    var data = $(this).data();
                    self.formAdd.load(data.recordKey);
                });
            }
        });
        self.tableMain.load();
    },
    load: function () {
        var self = main;
        self.control.tableMain.jtable('load', self.control.formMain.serialize());
    }
}

main.formAdd = {
    init: function () {
        var self = main;
        main.formAdd.event();
    },
    event: function () {
        var self = main;
        self.control.btnAdd.on('click', function (e) {
            e.preventDefault();
            self.control.formAdd[0].reset();
            self.control.formAdd.find('tbody').html("")
            self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').removeAttr('disabled', 'disabled');
           // self.control.formAdd.find('name="ACTIVITY_CODE"').disabled;
            self.control.formAdd.find('button[value=edit]').hide();
            self.control.formAdd.find('button[value=save]').show();
            self.control.modalAdd.modal('show');
        });
        self.control.formAdd.find('button[value=edit]').on('click', function (e) {
            e.preventDefault();
            self.control.formAdd.find('button[value=edit]').hide();
            self.control.formAdd.find('button[value=save]').show();
            self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').removeAttr('disabled', 'disabled');

        });
        self.control.formAdd.find('[name=PERIOD_CODE]').on('click', function (e) {
            e.preventDefault();
            var option = app.listModal.options.period;
            app.listModal.init(option);
            app.listModal.load(option);
            app.listModal.control.tempCallback = function (ret) {
                self.control.formAdd.find('[name=SITE_ID]').val(ret.SITE_ID).trigger('keyup');
                self.control.formAdd.find('[name=CORP_ID]').val(ret.CORP_ID).trigger('keyup');
                self.control.formAdd.find('[name=PERIOD_ID]').val(ret.PERIOD_ID).trigger('keyup');
                self.control.formAdd.find('[name=PERIOD_CODE]').val(ret.PERIOD_CODE).trigger('keyup');
            }
            app.listModal.show();
        });
        self.control.formAdd.find('[name=DEPT_CODE]').on('click', function (e) {
            e.preventDefault();
            var option = app.listModal.options.dept;
            app.listModal.init(option);
            app.listModal.load(option);
            app.listModal.control.tempCallback = function (ret) {
                self.control.formAdd.find('[name=DEPT_ID]').val(ret.DEPT_ID).trigger('keyup');
                self.control.formAdd.find('[name=DEPT_CODE]').val(ret.DEPT_CODE + "-" + ret.DEPT_NAME_EN).trigger('keyup');
            }
            app.listModal.show();
        });
        self.control.formAdd.find('[name=PROJ_CODE]').on('click', function (e) {
            e.preventDefault();
            var option = app.listModal.options.proj;
            app.listModal.init(option);
            app.listModal.load(option);
            app.listModal.control.tempCallback = function (ret) {
                self.control.formAdd.find('[name=PROJ_ID]').val(ret.PROJ_ID).trigger('keyup');
                self.control.formAdd.find('[name=PROJ_CODE]').val(ret.PROJ_CODE + "-" + ret.PROJ_NAME_EN).trigger('keyup');
            }
            app.listModal.show();
        });
        self.control.formAdd.find('[name=SECT_CODE]').on('click', function (e) {
            e.preventDefault();
            var option = app.listModal.options.sect;
            app.listModal.init(option);
            app.listModal.load(option);
            app.listModal.control.tempCallback = function (ret) {
                self.control.formAdd.find('[name=SECT_ID]').val(ret.SECT_ID).trigger('keyup');
                self.control.formAdd.find('[name=SECT_CODE]').val(ret.SECT_CODE + "-" + ret.SECT_NAME_EN).trigger('keyup');
            }
            app.listModal.show();
        });
        self.control.formAdd.find('[name=JOB_CODE]').on('click', function (e) {
            e.preventDefault();
            var option = app.listModal.options.job;
            app.listModal.init(option);
            app.listModal.load(option);
            app.listModal.control.tempCallback = function (ret) {
                self.control.formAdd.find('[name=JOB_ID]').val(ret.JOB_ID).trigger('keyup');
                self.control.formAdd.find('[name=JOB_CODE]').val(ret.JOB_CODE + "-" + ret.JOB_NAME_EN).trigger('keyup');
            }
            app.listModal.show();
        });
        self.control.formAdd.find('[name=TOTAL_BUDGET_AMT]').number(true, 2);

        self.control.formAdd.find('[name=groupAdd]').on('click', function (e) {
            if (self.control.formAdd.find('[name=ACTIVITY_ID]').val() == "")
            {
                self.formAdd.addGroup();
            }
            else
            {
                groupList.loadList();
            }
            
        });

        self.control.formAdd.sendForm({
            before: function () {

            },
            submit: function () {
                self.formAdd.save();
            },
            reset: function () {
                self.control.formAdd.find('button[value=edit]').show();
                self.control.formAdd.find('button[value=save]').hide();
                self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').attr('disabled', 'disabled');
            }
        });
        self.control.formAdd.find('[name=ADD_ITEM]').click(function (e) {
            e.preventDefault();
            self.control.formAdd.find('tbody').append(self.formAdd.row()).trigger('change');
            self.formAdd.rowSeq();
        });
        self.control.formAdd.find('tbody').sortable({
            cancel: '.disabled, input, select, button',
            update: function () {
                self.formAdd.rowSeq();
            }
        });

    },
    rowChange: function (tr) {
        var budget = parseInt( tr.find('[name=BUDGET_AMT]').val());
        var tbudget = parseInt(main.control.formAdd.find('[name=TOTAL_BUDGET_AMT]').val());
        var totalBudget = tbudget + budget;
        main.control.formAdd.find('[name=TOTAL_BUDGET_AMT]').val(totalBudget);
        
    },
    sumAll: function () {
        var self = main;
        var sum = 0.00;
        self.control.formAdd.find('tbody [name=BUDGET_AMT]').each(function (i, e) {
            sum += parseFloat($(e).val());
        });
        self.control.formAdd.find('[name=TOTAL_BUDGET_AMT]').val(sum).trigger('change');
    },
    rowSeq: function () {
        var self = main;
        self.control.formAdd.find('tbody tr').each(function (i, e) {
            $(e).find('[data-seq=SEQ]').text(i + 1);
            $(e).find('[name=SEQ]').val(i + 1);
        });
    },
    rowSelectedItem: function (tr) {
        app.listModal.reset();
        var self = main;
        var option = app.listModal.options.item;
        option.ACTIVITY_ID = self.ACTIVITY_ID;
        app.listModal.init(option);
        app.listModal.load(option);
        app.listModal.control.tempCallback = function (data) {
            $.each(data, function (i, e) {
                    tr.find('[name=' + i + ']').val(e);
            });
            app.listModal.control.tempModal.modal('hide');
        };
        app.listModal.control.tempModal.modal('show');
    },
    row: function (read, obj) {
        var self = main;
        var tr = $('<tr>').html($('#rowTemplate').html());
        tr.find('[name=BUDGET_AMT]').on('keyup', function (e) {
            e.preventDefault();
            //self.formAdd.rowChange(tr);
            self.formAdd.sumAll();
        });
        setTimeout(function () {
            tr.find('[name=BUDGET_AMT]').number(true, 2);
            tr.find('button[name=REMOVE_RECORD]').on('click', function () {
                if (self.control.formAdd.find('tbody tr').length <= 1) {
                    app.dialogue.warning('Message from system.', 'Minimum 1 row.');
                } else {
                    tr.remove();
                    self.formAdd.rowSeq();
                }
            });
            $('tr').find('[name=BUDGET_CHART_CODE]').click(function () {
                self.formAdd.chartCode($(this).closest('tr')); //closest('tr') เป็นการถอยมาถึง tr เพื่อให้มันหาตัวมันเองเจอเพราะมันจะเรียกตัวที่ถอยลงมาจากตัวมันเอง
            });
            
            $('tr').find('[name=ACTIVITY_GROUP_CODE]').click(function () {
                //self.formAdd.rowSelectedItem(tr);
                self.control.formAdd.find('[name=statusGroup]').val('1').trigger('change');
                groupList.loadList();
                self.formAdd.groupCode($(this).closest('tr'));
                
            });
        }, 1)
        if (obj != undefined) {
            $.each(obj, function (n, v) {
                tr.find('[name=' + n + ']').val(v);
            });
        }
        setTimeout(function () {
            tr.data(obj);
            if (read) {
                tr.addClass('disabled');
                tr.find('input, button').addClass('disabled').attr('disabled', 'disabled');
            }
        }, 2)
        return tr;
    },
    chartCode: function (element) {
        var option = app.listModal.options.chart;
        app.listModal.init(option);
        app.listModal.load(option);
        app.listModal.control.tempCallback = function (ret) {
            element.find('[name=BUDGET_CHART_ID]').val(ret.BUDGET_CHART_ID).trigger('keyup');
            element.find('[name=BUDGET_CHART_CODE]').val(ret.BUDGET_CHART_CODE).trigger('keyup');
        }
        app.listModal.show();
    },
    groupCode: function (elementG) {
        var self = main;
        //var option = app.listModal.options.group;
        //app.listModal.init(option);
        //app.listModal.load(option);
        //app.listModal.control.tempCallback = function (ret) {
        //    elementG.find('[name=ACTIVITY_GROUP_ID]').val(ret.ACTIVITY_GROUP_ID).trigger('keyup');
        //    elementG.find('[name=ACTIVITY_GROUP_CODE]').val(ret.ACTIVITY_GROUP_CODE).trigger('keyup');
        //    main.control.formAdd.find('[name=statusGroup]').val(""); }
        //       app.listModal.show();
        self.control.tempCallback = function (retG) {
            elementG.find('[name=ACTIVITY_GROUP_ID]').val(retG.ACTIVITY_GROUP_ID).trigger('keyup');
            elementG.find('[name=ACTIVITY_GROUP_CODE]').val(retG.ACTIVITY_GROUP_CODE).trigger('keyup');
            main.control.formAdd.find('[name=statusGroup]').val("");
            groupList.control.modal.select.modal("hide");
        }

        //if (self.control.formAdd.find('tbody [name=ACTIVITY_ITEM_CODE]').val() != "") {
        //    self.control.formAdd.find('tbody [name=ACTIVITY_ITEM_CODE]').each(function (i, e) {

        //    });
        //}
        //else {
        //  elementG.find('[name=ACTIVITY_ITEM_CODE]').val(elementG.find('[name=ACTIVITY_GROUP_CODE]').val() + "-"+ "000" ).trigger('keyup'); 
        //}
        
    },
    load: function (id) {
        var self = main;
        main.control.formAdd[0].reset();
        self.control.formAdd.find('div.overlay').show();
        self.control.modalAdd.modal('show');
        self.control.formAdd.find('tbody').html("");
        $.ajax({
            url: '/Api/Activity/' + id,
            type: 'GET',
            success: function (ret) {
                if (ret.Result === "OK") {
                    $.each(ret.Header, function (i, e) {
                        self.control.formAdd.find('[name=' + i + ']').val(e).text(e);
                    });
                    setTimeout(function () {
                        $.each(ret.Detail, function (i, e) {
                            self.formAdd.row(true, e).appendTo(self.control.modalAdd.find('tbody'));
                        });
                        setTimeout(function () {
                            self.formAdd.rowSeq();
                        }, 1);
                        self.control.formAdd.find('div.overlay').hide();
                    }, 0);
                } else {
                    dialogue.warning('', ret.Message, function () {
                        self.control.formAdd.modal('hide');
                    });
                }
                self.control.formAdd.find('button[value=edit]').show();
                self.control.formAdd.find('button[value=save]').hide();
                self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').attr('disabled', 'disabled');
                self.control.formAdd.find('div.overlay').hide();
            }
        })
    },
    loadGroup: function (id) {
        var self = main;
        main.control.formAdd[0].reset();
        self.control.formAdd.find('div.overlay').show();
        self.control.modalAdd.modal('show');
        self.control.formAdd.find('tbody').html("");
        $.ajax({
            url: '/Api/Activity/Group/' + id,
            type: 'GET',
            success: function (ret) {
                if (ret.Result === "OK") {
                    $.each(ret.Header, function (i, e) {
                        self.control.formAdd.find('[name=' + i + ']').val(e).text(e);
                    });
                    setTimeout(function () {
                        $.each(ret.Detail, function (i, e) {
                            self.formAdd.row(false, e).appendTo(self.control.modalAdd.find('tbody'));
                        });
                        setTimeout(function () {
                            self.formAdd.rowSeq();
                        }, 1);
                        self.control.formAdd.find('div.overlay').hide();
                    }, 0);
                } else {
                    dialogue.warning('', ret.Message, function () {
                        self.control.formAdd.modal('hide');
                    });
                }
                self.control.formAdd.find('button[value=edit]').hide();
                self.control.formAdd.find('button[value=save]').show();
                self.control.formAdd.find('input, select, textarea, button:not(button[value=edit],button[download=true],button[value=save],button[value=close])').removeAttr('disabled', 'disabled');
                self.control.formAdd.find('div.overlay').hide();
            }
        })
    },
    save: function () {
        var self = main;
        self.control.formAdd.find('div.overlay').show();
        $.ajax({
            url: '/Api/Activity/Save',
            data: self.control.formAdd.formToJson("DETAIL", "tbody tr"),
            success: function (ret) {
                if (ret.Result === "OK") {
                    dialogue.success();
                    self.tableMain.load();
                    self.formAdd.load(ret.Data);
                } else {
                    dialogue.warning('', ret.Message, function () {
                        self.control.formAdd.modal('hide');
                    });
                }
                self.control.formAdd.find('div.overlay').hide();
            }
        });
    },
    addGroup: function () {
        var self = main;
        self.control.formAdd.find('div.overlay').show();
        $.ajax({
            url: '/Api/Activity/AddGroup',
            data: self.control.formAdd.formToJson(),
            success: function (ret) {
                if (ret.Result === "OK") {
                    self.tableMain.load();
                    self.formAdd.loadGroup(ret.Data);
                } else {
                    dialogue.warning('', ret.Message, function () {
                        self.control.formAdd.modal('hide');
                    });
                }
                self.control.formAdd.find('div.overlay').hide();
            }
        });
    }
}


var groupList = {
    init: function () {
        groupList.control.table.jtable({
            paging: true,
            pageSize: 10,
            sorting: true,
            saveUserPreferences: false,
            defaultSorting: 'ACTIVITY_GROUP_CODE ASC',
            actions: {
                listAction: '/Api/Activity/ListGroup',
            },
            fields: {
                ACTIVITY_GROUP_ID: {
                    key: true,
                    list: false,
                },
                ACTIVITY_GROUP_CODE: {
                    title: 'CODE',
                    width: "8%",
                    listClass: "ACTIVITY_GROUP_CODE"
                },
                ACTIVITY_GROUP_NAME: {
                    title: 'NAME',
                    width: "30%",
                    listClass: "ACTIVITY_GROUP_NAME"
                }
            },
            recordsLoaded: function (event, data) {
                $('#GroupListTableContainer .jtable-data-row td').not('.MENU').click(function (e) {
                    e.preventDefault();
                    var obj = {
                        ACTIVITY_GROUP_ID: $(this).closest('tr').attr('data-record-key').trim(),
                        ACTIVITY_GROUP_CODE: $(this).closest('tr').find("td.ACTIVITY_GROUP_CODE").text().trim(),
                        ACTIVITY_GROUP_NAME: $(this).closest('tr').find("td.ACTIVITY_GROUP_NAME").text().trim()
                       
                    };
                    if (main.control.formAdd.find('[name=statusGroup]').val() == "")
                    {
                        main.formAdd.row(false, obj).appendTo(main.control.modalAdd.find('tbody'));
                        main.formAdd.rowSeq();
                        groupList.control.modal.select.modal("hide");
                    }
                    else {
                        main.control.tempCallback(obj);
                        /*
                        main.control.formAdd.find('[name=ACTIVITY_GROUP_ID]').val(obj.ACTIVITY_GROUP_ID).trigger('keyup');
                        main.control.formAdd.find('[name=ACTIVITY_GROUP_CODE]').val(obj.ACTIVITY_GROUP_CODE).trigger('keyup');
                        main.control.formAdd.find('[name=statusGroup]').val("");
                        groupList.control.modal.select.modal("hide");
                        */
                    }
                    
                });
            }
        });
        groupList.control.form.select.submit(function (e) {
            e.preventDefault();
            $('#GroupListTableContainer').jtable('load', { ACTIVITY_ID: $('#modalAdd [name="ACTIVITY_ID"]').val(), SEARCH: $('#GroupListModalSearch [name=SEARCH]').val() });
        });
        groupList.event();
    },
    control: {
        modal: {
            add: $('#modal-add-Group'),
            select: $('#modal-select-Group'),
        },
        form: {
            add: $('#modal-add-Group form'),
            select: $('#modal-select-Group form'),
        },
        table: $('#GroupListTableContainer'),
        formAddGroup: $('#formAddGroup')
    },
    event: function () {

        groupList.control.modal.add.find('button[type=submit][value=add]').click(function (e) {
            e.preventDefault();
            //if (groupList.formAddGroup.find('[name=ACTIVITY_GROUP_CODE]').val().length != 2) {

            //}
            groupList.save();
            
        });
        
        groupList.control.modal.select.find('button[type=button][value=add]').click(function (e) {
            e.preventDefault();
            groupList.control.form.add[0].reset();
            groupList.control.form.add.find('[name=ACTIVITY_ID]').val(main.control.formAdd.find('[name=ACTIVITY_ID]').val())
            groupList.control.form.add.find('[name="SITE_ID"]').val(main.control.formAdd.find('[name="SITE_ID"]').val());
           
            groupList.control.table.find('.jtable').each(function (i, e) {
                groupList.control.formAddGroup.find('[name=SEQG]').val(i + 1);
            }); 
            groupList.control.modal.add.modal("show");
        });

       // groupList.control.formAddGroup.find('[name=ACTIVITY_GROUP_CODE]').number(true);
    },
    load: function () {
        var self = groupList;
        self.control.table.jtable('load', main.control.formMain.serialize());
        
    },
    loadList: function () {
        groupList.control.form.select[0].reset();
        groupList.control.table.jtable('load', { ACTIVITY_ID: $('#modalAdd [name="ACTIVITY_ID"]').val(), SEARCH: groupList.control.form.select.find("[name=SEARCH]").val() });
        groupList.control.modal.select.modal("show");

       
    },
    loadListForm: function () {
        groupList.control.table.jtable('load', { ACTIVITY_ID: $('#modalAdd [name="ACTIVITY_ID"]').val(), SEARCH: groupList.control.form.select.find("[name=SEARCH]").val() });
        groupList.control.modal.select.modal("show");
    },
    save: function () {
        var self = groupList;
        self.control.modal.add.find('div.overlay').show();
            $.ajax({
                url: '/Api/Activity/AddActGroup',
                data: self.control.formAddGroup.formToJson(),
                success: function (ret) {
                    if (ret.Result === "OK") {
                        dialogue.success();
                        self.load();
                        self.control.modal.select.modal("hide");
                        self.control.modal.add.modal('hide');
                    } else {
                        dialogue.warning('', ret.Message, function () {
                            self.control.modal.add.modal('hide');
                        });
                    }
                    self.control.modal.add.find('div.overlay').hide();
                }
            });
    }
}

$(document).ready(function () {
    main.init();
});