﻿var dialogue = {
    warning: function (title, text, callback) {
        if (title == "" || title == undefined) {
            title = "Message from system.";
        }
        if (text == "" || text == undefined) {
            text = "Are you sure to continue?";
        }
        $.confirm({
            title: title,
            text: text,
            confirm: function () {
                if ($.isFunction(callback)) {
                    callback();
                }
            },
            confirmButton: 'Close',
            confirmButtonClass: 'btn-warning btn-flat',
            cancelButtonClass: 'hidden'
        })
    },
    warningConfirm: function (title, text, callback) {
        if (title == "" || title == undefined) {
            title = "Message from system.";
        }
        if (text == "" || text == undefined) {
            text = "Are you sure to continue?";
        }
        $.confirm({
            title: title,
            text: text,
            confirm: function () {
                if ($.isFunction(callback)) {
                    callback();
                }
            },
            confirmButtonClass: 'btn-warning btn-flat',
            cancelButtonClass: 'btn-flat'
        })
    },
    success: function (title, text, callback) {
        if (title == "" || title == undefined) {
            title = "Message from system.";
        }
        if (text == "" || text == undefined) {
            text = "Success!";
        }
        $.confirm({
            title: title,
            text: text,
            confirm: function () {
                if ($.isFunction(callback)) {
                    callback();
                }
            },
            confirmButton: 'Close',
            confirmButtonClass: 'btn-success btn-flat',
            cancelButtonClass: 'hidden'
        })
    },
    successConfirm: function (title, text, callback) {
        if (title == "" || title == undefined) {
            title = "Message from system.";
        }
        if (text == "" || text == undefined) {
            text = "Success!";
        }
        $.confirm({
            title: title,
            text: text,
            confirm: function () {
                if ($.isFunction(callback)) {
                    callback();
                }
            },
            confirmButtonClass: 'btn-success btn-flat',
            cancelButtonClass: 'btn-flat'
        })
    }
};

var app = {
    list: {
        DocumentrequirementTypes: function (callback) {
            $.ajax({
                url: '/Api/Document/List/RequirementTypes',
                type: 'POST',
                success: function (ret) {
                    if ($.isFunction(callback)) {
                        callback(ret);
                    }
                }
            });
        },
        DocumentPrioritys: function (callback) {
            $.ajax({
                url: '/Api/Document/List/Prioritys',
                type: 'POST',
                success: function (ret) {
                    if ($.isFunction(callback)) {
                        callback(ret);
                    }
                }
            })
        },
        DocumentStatus: function (callback, optional) {
            $.ajax({
                url: '/Api/Document/List/Status' + (optional == undefined ? "" : "/" + optional),
                type: 'POST',
                success: function (ret) {
                    if ($.isFunction(callback)) {
                        callback(ret);
                    }
                }
            })
        },
        ProjectStatus: function (callback) {
            $.ajax({
                url: '/Api/Project/List/Status',
                type: 'POST',
                success: function (ret) {
                    if ($.isFunction(callback)) {
                        callback(ret);
                    }
                }
            })
        },
        userDeveloper: function (callback) {
            $.ajax({
                url: '/Api/User/List/Developer',
                type: 'POST',
                success: function (ret) {
                    if ($.isFunction(callback)) {
                        callback(ret);
                    }
                }
            })
        },
        userSA: function (callback) {
            $.ajax({
                url: '/Api/User/List/SA',
                type: 'POST',
                success: function (ret) {
                    if ($.isFunction(callback)) {
                        callback(ret);
                    }
                }
            })
        },
        userTester: function (callback) {
            $.ajax({
                url: '/Api/User/List/TESTER',
                type: 'POST',
                success: function (ret) {
                    if ($.isFunction(callback)) {
                        callback(ret);
                    }
                }
            })
        },
    },
    listModal: {
        init: function (option) {
            app.listModal.control.tempForm[0].reset();
            app.listModal.control.tempModal.find('.modal-title').text(option.title);
            if (app.listModal.control.tempTable.find('table').length > 0) {
                app.listModal.control.tempTable.jtable('destroy');
            }
            app.listModal.control.tempTable.jtable({
                paging: true,
                pageSize: 10,
                sorting: true,
                saveUserPreferences: false,
                defaultSorting: option.orderby,
                actions: {
                    listAction: option.listUrl,
                },
                fields: option.fields,
                recordsLoaded: function (event, data) {
                    app.listModal.control.tempModal.find('div.overlay').hide();
                    option.recordsLoaded(event, data);
                }
            });
            app.listModal.control.tempForm.submit(function (e) {
                e.preventDefault();
                app.listModal.load(option);
            });
        },
        control: {
            tempModal: $('#tempModal'),
            tempForm: $('#tempForm'),
            tempTable: $('#tempListTableContainer'),
            tempElement1: undefined,
            tempElement2: undefined,
            tempCallback: undefined
        },
        load: function (option) {
            app.listModal.control.tempModal.find('div.overlay').show();
            option.load();
        },
        options: {
            site: {
                listUrl: '/Api/Site/List',
                orderby: 'SITE_CODE ASC',
                title: 'Site List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    SITE_ID: {
                        list: false,
                        key: true
                    },
                    SITE_CODE: {
                        title: 'Site Code',
                        width: "10%"
                    },
                    SITE_NAME: {
                        title: 'Site Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=SITE_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=SITE_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.SITE_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.SITE_CODE + " - " + data.SITE_NAME).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            corp: {
                listUrl: '/Api/Corp/List',
                orderby: 'CORP_CODE ASC',
                title: 'Corp List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    SITE_ID: {
                        list: false,
                        key: true
                    },
                    CORP_ID: {
                        list: false,
                        key: true
                    },
                    CORP_CODE: {
                        title: 'Corp Code',
                        width: "10%"
                    },
                    CORP_NAME: {
                        title: 'Corp Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=SITE_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=CORP_ID]');
                    }
                    if (self.listModal.control.tempElement3 == undefined) {
                        self.listModal.control.tempElement3 = $('div.modal.fade.in form input[name=CORP_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.SITE_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.CORP_ID).trigger('keyup');
                            self.listModal.control.tempElement3.val(data.CORP_CODE + " - " + data.CORP_NAME).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            dept: {
                listUrl: '/Api/Dept/List',
                orderby: 'DEPT_CODE ASC',
                title: 'Dept List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    SITE_ID: {
                        list: false,
                        key: true
                    },
                    CORP_ID: {
                        list: false,
                        key: true
                    },
                    DEPT_ID: {
                        list: false,
                        key: true
                    },
                    DEPT_CODE: {
                        title: 'Dept Code',
                        width: "10%"
                    },
                    DEPT_NAME_EN: {
                        title: 'Dept Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=SITE_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=CORP_ID]');
                    }
                    if (self.listModal.control.tempElement3 == undefined) {
                        self.listModal.control.tempElement3 = $('div.modal.fade.in form input[name=DEPT_ID]');
                    }
                    if (self.listModal.control.tempElement4 == undefined) {
                        self.listModal.control.tempElement4 = $('div.modal.fade.in form input[name=DEPT_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.SITE_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.CORP_ID).trigger('keyup');
                            self.listModal.control.tempElement3.val(data.DEPT_ID).trigger('keyup');
                            self.listModal.control.tempElement4.val(data.DEPT_CODE + " - " + data.DEPT_NAME_EN).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            proj: {
                listUrl: '/Api/Proj/List',
                orderby: 'PROJ_CODE ASC',
                title: 'Proj List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    SITE_ID: {
                        list: false,
                        key: true
                    },
                    CORP_ID: {
                        list: false,
                        key: true
                    },
                    PROJ_ID: {
                        list: false,
                        key: true
                    },
                    PROJ_CODE: {
                        title: 'Proj Code',
                        width: "10%"
                    },
                    PROJ_NAME_EN: {
                        title: 'Proj Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=SITE_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=CORP_ID]');
                    }
                    if (self.listModal.control.tempElement3 == undefined) {
                        self.listModal.control.tempElement3 = $('div.modal.fade.in form input[name=PROJ_ID]');
                    }
                    if (self.listModal.control.tempElement4 == undefined) {
                        self.listModal.control.tempElement4 = $('div.modal.fade.in form input[name=PROJ_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.SITE_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.CORP_ID).trigger('keyup');
                            self.listModal.control.tempElement3.val(data.PROJ_ID).trigger('keyup');
                            self.listModal.control.tempElement4.val(data.PROJ_CODE + " - " + data.PROJ_NAME_EN).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            period: {
                listUrl: '/Api/Period/List',
                orderby: 'PERIOD_CODE ASC',
                title: 'Period List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    SITE_ID: {
                        list: false,
                        key: true
                    },
                    CORP_ID: {
                        list: false,
                        key: true
                    },
                    PERIOD_ID: {
                        list: false,
                        key: true
                    },
                    PERIOD_CODE: {
                        title: 'period Code',
                        width: "10%"
                    },
                    PERIOD_NAME_EN: {
                        title: 'period Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=SITE_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=CORP_ID]');
                    }
                    if (self.listModal.control.tempElement3 == undefined) {
                        self.listModal.control.tempElement3 = $('div.modal.fade.in form input[name=PERIOD_ID]');
                    }
                    if (self.listModal.control.tempElement4 == undefined) {
                        self.listModal.control.tempElement4 = $('div.modal.fade.in form input[name=PERIOD_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.SITE_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.CORP_ID).trigger('keyup');
                            self.listModal.control.tempElement3.val(data.PERIOD_ID).trigger('keyup');
                            self.listModal.control.tempElement4.val(data.PERIOD_CODE).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            sect: {
                listUrl: '/Api/Sect/List',
                orderby: 'SECT_CODE ASC',
                title: 'Sect List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    SECT_ID: {
                        list: false,
                        key: true
                    },
                    SECT_CODE: {
                        title: 'Sect Code',
                        width: "10%"
                    },
                    SECT_NAME_EN: {
                        title: 'Sect Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=SECT_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=SECT_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.SECT_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.SECT_CODE + " - " + data.SECT_NAME_EN).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            job: {
                listUrl: '/Api/Job/List',
                orderby: 'JOB_CODE ASC',
                title: 'Job List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    JOB_ID: {
                        list: false,
                        key: true
                    },
                    JOB_CODE: {
                        title: 'Sect Code',
                        width: "10%"
                    },
                    JOB_NAME_EN: {
                        title: 'Sect Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=JOB_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=JOB_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.JOB_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.JOB_CODE + " - " + data.JOB_NAME_EN).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            chart: {
                listUrl: '/Api/Budget_Chart/List',
                orderby: 'BUDGET_CHART_CODE ASC',
                title: 'Chart List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    BUDGET_CHART_ID: {
                        list: false,
                        key: true
                    },
                    BUDGET_CHART_CODE: {
                        title: 'Chart Code',
                        width: "10%"
                    },
                    BUDGET_CHART_NAME_EN: {
                        title: 'Chart Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=BUDGET_CHART_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=BUDGET_CHART_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.BUDGET_CHART_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.BUDGET_CHART_CODE + " - " + data.BUDGET_CHART_NAME_EN).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            group: {
                listUrl: '/Api/Activity/ListGroup',
                orderby: 'ACTIVITY_GROUP_CODE ASC',
               // data: ACTIVITY_ID,
                title: 'Group List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    ACTIVITY_GROUP_ID: {
                        list: false,
                        key: true
                    },
                    ACTIVITY_GROUP_CODE: {
                        title: 'Group Code',
                        width: "10%"
                    },
                    ACTIVITY_GROUP_NAME: {
                        title: 'Group Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=ACTIVITY_GROUP_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=ACTIVITY_GROUP_CODE]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.ACTIVITY_GROUP_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.ACTIVITY_GROUP_CODE).trigger('keyup');
                        }
                        self.listModal.control.tempModal.modal('hide');
                    });
                },
                load: function () {
                    app.listModal.control.tempTable.jtable('load', app.listModal.control.tempForm.serialize());
                }
            },
            item: {
                listUrl: '/Api/Activity/ListGroup',
                orderby: 'ACTIVITY_GROUP_CODE ASC',
                modalID: '',
                title: 'Group List',
                fields: {
                    RECORD: {
                        title: '#',
                        sorting: false,
                        width: "3%",
                        align: "center"
                    },
                    ACTIVITY_GROUP_ID: {
                        list: false,
                        key: true
                    },
                    ACTIVITY_GROUP_CODE: {
                        title: 'Group Code',
                        width: "10%"
                    },
                    ACTIVITY_GROUP_NAME: {
                        title: 'Group Name',
                    }
                },
                recordsLoaded: function (event, data) {
                    var self = app;
                    if (self.listModal.control.tempElement1 == undefined) {
                        self.listModal.control.tempElement1 = $('div.modal.fade.in form input[name=ACTIVITY_GROUP_ID]');
                    }
                    if (self.listModal.control.tempElement2 == undefined) {
                        self.listModal.control.tempElement2 = $('div.modal.fade.in form input[name=ACTIVITY_GROUP_CODE]');
                    }
                    if (self.listModal.control.tempElement3 == undefined) {
                        self.listModal.control.tempElement3 = $('div.modal.fade.in form input[name=ACTIVITY_GROUP_CODE]');
                    }
                    if (self.listModal.control.tempElement4 == undefined) {
                        self.listModal.control.tempElement4 = $('div.modal.fade.in form input[name=ACTIVITY_GROUP_NAME]');
                    }
                    self.listModal.control.tempTable.find('.jtable-data-row').click(function (e) {
                        var data = $(this).data("record");
                        if ($.isFunction(self.listModal.control.tempCallback)) {
                            self.listModal.control.tempCallback(data);
                        } else {
                            self.listModal.control.tempElement1.val(data.ACTIVITY_ID).trigger('keyup');
                            self.listModal.control.tempElement2.val(data.ACTIVITY_GROUP_CODE + " - " + data.ACTIVITY_GROUP_NAME).trigger('keyup');
                            self.listModal.control.tempElement3.val(data.ACTIVITY_GROUP_CODE).trigger('keyup');
                            self.listModal.control.tempElement4.val(data.ACTIVITY_GROUP_NAME).trigger('keyup');
                            self.listModal.control.tempModal.modal('hide');
                        }
                    });
                },
                load: function () {
                    var self = this;
                    app.listModal.control.tempTable.jtable('load', {
                        SEARCH: app.listModal.control.tempForm.find('[name=SEARCH]').val(),
                        ACTIVITY_ID: self.ACTIVITY_ID
                    });
                }
            }
        },
        show: function () {
            app.listModal.control.tempModal.modal('show');
        },
        hide: function () {
            app.listModal.control.tempModal.modal('hide');
        },
        reset: function () {
            var self = this;
            self.control = {
                tempModal: $('#tempModal'),
                tempForm: $('#tempForm'),
                tempTable: $('#tempListTableContainer'),
                tempElement1: undefined,
                tempElement2: undefined,
                tempElement3: undefined,
                tempElement4: undefined,
                tempCallback: undefined
            };
        }

    },
    getParameterByName: function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    dialogue: dialogue,
    date: {
        firstday: function (isDateTime) {
            return isDateTime ? moment().date(1).format("DD/MM/YYYY HH:mm") : moment().date(1).format("DD/MM/YYYY");
        },
        lastday: function (isDateTime) {
            return isDateTime ? moment().add(1, 'M').date(0).format("DD/MM/YYYY HH:mm") : moment().add(1, 'M').date(0).format("DD/MM/YYYY");
        },
        today: function (isDateTime) {
            return isDateTime ? moment().format("DD/MM/YYYY HH:mm") : moment().format("DD/MM/YYYY");
        }
    },
    guid: function () {
        function S4() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        }
        return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toUpperCase();
    },
    initNumber: function () {
        $(".number, .currency").unbind("keydown");
        try {
            $(".number").number(true, 0);
            $(".currency").number(true, 2);
        } catch (e) {
            $(".currency").number(true, 2);
        }
    },
    initDateTime: function () {
        var locale = 'th';
        $('.datepicker').datetimepicker({
            locale: locale,
            ignoreReadonly: true,
            widgetParent: $(this).parent().prop('tagName') == "TD" ? $(this).parent()[0] : $(this).closest('div.modal').length > 0 ? $(this).closest('div.modal')[0] : null,
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
        $('.timepicker').datetimepicker({
            locale: locale,
            ignoreReadonly: true,
            format: 'HH:mm',
            widgetParent: $(this).parent().prop('tagName') == "TD" ? $(this).parent()[0] : null,
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
        $('.datetimepicker').datetimepicker({
            locale: locale,
            ignoreReadonly: true,
            format: "DD/MM/YYYY HH:mm",
            showTodayButton: true,
            widgetParent: $(this).parent().prop('tagName') == "TD" ? $(this).parent()[0] : null,
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
    },
    userData: {
        USER_ID: $('#USER_DATA [name=USER_ID]').val(),
        ROLE_ID: $('#USER_DATA [name=ROLE_ID]').val(),
    },
    allowedExtensions: ['jpeg', 'jpg', 'gif', 'png', 'bmp', 'txt', 'zip', 'docx', 'xlsx', 'pdf', 'pptx', 'csv', 'sql', 'rar']
}

$(document).ready(function () {
    //if (Cookies("access_token") == undefined) {
    $.ajaxSetup({
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        dataType: "json",
        type: "POST",
    });
    //} else {
    //    $.ajaxSetup({
    //        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
    //        dataType: "json",
    //        type: "POST",
    //        headers: { "Authorization": "Bearer " + Cookies("access_token") }
    //    });
    //}
    moment.locale('th');
    app.initDateTime();
    app.initNumber();
})
    .ajaxStart(function (e, xhr, settings) {
        // $("div#loader").show();
    })
    .ajaxComplete(function (e, xhr, settings) {
        $("div#loader").hide();
    })
    .ajaxError(function (e, xhr, settings) {
        if (xhr.status == 401) {
            //window.location.href = '/Account/Index';
        }
    }).ajaxSuccess(function (e, xhr, settings) {
        $("div#loader").hide();
    }).on('shown.bs.modal', function () {
        $(this).find('[autofocus]').focus();
    }).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });