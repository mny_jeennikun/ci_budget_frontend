﻿/*!
 * Mashiro v0.0.1 (http:///)
 * Author: Nunthachai Noisanti
 * Website: 
 * Contact: 
 *
 * Copyright 2016
 * Licensed under
 */

if (typeof jQuery === 'undefined') {
    throw new Error('Mashiro requires jQuery');
}
(function ($) {
    $.fn.sendForm = function (options) {
        var self = this;
        var defaults = {
            before: function () {

            },
            submit: function (self) {

            },
            reset: function () {

            }
        };
        var options = $.extend(defaults, options);
        var _numberFormat = function () {
            obj = $(self).find('input[validate]');
            $(self).find('input[validate=number]').each(function (i, subElement) {
                var digit = $(subElement).attr('digit') != undefined ? parseInt($(subElement).attr('digit')) : 2;
                $(subElement).number(true, digit);
            });
        }
        var _validator = function () {
            var element = $(self).find('[required]:not([required="undefined"])');
            var i = $('<i/>').addClass('form-control-feedback fa').appendTo(element.parent());
            element.on('focus keyup change', function (x) {
                x.preventDefault();
                var e = $(this);
                var attr = $(this).attr('disabled');
                if (typeof attr !== typeof undefined && attr !== false) {
                    e.parent().removeClass('has-success');
                    e.parent().removeClass('has-error');
                    e.parent().find("i.form-control-feedback").removeClass("fa-check");
                    e.parent().find("i.form-control-feedback").removeClass("fa-remove");
                    return false;
                }
                var min = e.attr('minlength') == undefined ? false : !(parseInt(e.attr('minlength')) <= e.val().length);
                if (e.attr('confirm') != undefined) {
                    var confirm = e.closest('form').find('[name=' + e.attr('confirm') + ']');
                    e.parent().removeClass('has-success');
                    e.parent().addClass('has-error');
                    e.parent().find("i.form-control-feedback").removeClass("fa-check");
                    e.parent().find("i.form-control-feedback").addClass("fa-remove");
                    confirm.parent().removeClass('has-success');
                    confirm.parent().addClass('has-error');
                    confirm.parent().find("i.form-control-feedback").removeClass("fa-check");
                    confirm.parent().find("i.form-control-feedback").addClass("fa-remove");
                }
                if (e.prop('tagName') == "SELECT" && e.val() == "") {
                    e.parent().removeClass('has-success');
                    e.parent().addClass('has-error');
                    e.parent().find("i.form-control-feedback").removeClass("fa-check");
                    e.parent().find("i.form-control-feedback").addClass("fa-remove");
                } else if (e.prop('tagName') != "SELECT" && e.val().length === 0 || min) {
                    e.parent().removeClass('has-success');
                    e.parent().addClass('has-error');
                    e.parent().find("i.form-control-feedback").removeClass("fa-check");
                    e.parent().find("i.form-control-feedback").addClass("fa-remove");
                } else {
                    e.parent().removeClass('has-error');
                    e.parent().addClass('has-success');
                    e.parent().find("i.form-control-feedback").removeClass("fa-remove");
                    e.parent().find("i.form-control-feedback").addClass("fa-check");
                }
            });
            $(self).off('clear').on('clear', function (x) {
                x.preventDefault();
                element.closest('div').removeClass("has-success has-error");
                i.removeClass("fa-remove fa-check");
            });
        }
        var _reset = function () {
            $(self).off('reset').on('reset', function (e) {
                $(self).find('textarea').text("");
                $(self).find('[required]:not([required="undefined"])').parent().removeClass('has-error').removeClass('has-success')
                    .find("i.form-control-feedback").removeClass("fa-remove").removeClass("fa-check");
                options.reset();
            });
        }
        var _submit = function () {
            $(self).attr('novalidate', 'novalidate');
            $(self).submit(function (e) {
                e.preventDefault();
                $(self).find('button[type=submit]').attr('disabled', 'disabled');
                options.before();
                $(self).find('[required]:not([required="undefined"])').trigger('keyup');
                if ($(self).find("i.form-control-feedback.fa-remove").length > 0) {
                    $(self).find('button[type=submit]').removeAttr('disabled');
                } else {
                    options.submit(this);
                    $(self).find('button[type=submit]').removeAttr('disabled');
                }
            });
        }

        var _initFunction = function () {
            _numberFormat();
            _validator();
            _reset();
            _submit();
        }
        return _initFunction();
    }


    $.fn.formToJson = function (DETAIL_NAME, ELEMENT_GROUP) {
        var self = $(this).find('[name]:not([disabled])').not('div[detailform] [name]');
        var obj = {};
        $.each(self, function (i, e) {
            if ($(e).prop('tagName') != "BUTTON") {
                obj[$(e).attr('name')] = e.value;
            }
        });
        
        //if (DETAIL_NAME != "" && DETAIL_NAME != undefined) {
        //    var detail = $(this).find('div[detailform=' + DETAIL_NAME + '] div.form-group');
        //    var objDetail = [];
        //    $.each(detail, function (i, e) {
        //        var item = {};
        //        $.each($(e).find('[name]'), function (x, y) {
        //            item[$(y).attr('name')] = y.value;
        //        });
        //        objDetail.push(item);
        //    });
        //    obj[DETAIL_NAME] = objDetail;
        //}
        //return obj;

        if (DETAIL_NAME != "" && DETAIL_NAME != undefined) {
            var objDetail = [];
            var detail = $(this).find('div[detailform=' + DETAIL_NAME + ']');
            if (ELEMENT_GROUP != "" && ELEMENT_GROUP != undefined) {
                detail = $(this).find('div[detailform=' + DETAIL_NAME + '] ' + ELEMENT_GROUP);
            }
            $.each(detail, function (i, e) {
                var item = {};
                $.each($(e).find('[name]:not(button,[disabled])'), function (x, y) {
                    item[$(y).attr('name')] = $(y).val();
                });
                objDetail.push(item);
            });
            obj[DETAIL_NAME] = objDetail;
        }
        return obj;
    }


})(jQuery);